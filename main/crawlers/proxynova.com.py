import html
import re

import requests


def run_crawler():
    res = requests.get('https://www.proxynova.com/proxy-server-list/country-br/')
    data = res.text
    tree = html.fromstring(data)
    trs = tree.xpath("//tr[@data-proxy-id]")

    proxies = []

    for tr in trs:
        proxy_url = proxy_port = proxy_location = None
        try:
            proxy_url = tr[0][0].get('title')
            proxy_port = re.search('Port (\d+) proxies', tr[1][0].get('title')).group(1)
            proxy_location = tr[5][1][0].text.strip(' - ')
        except:
            pass

        if proxy_location:
            proxies.append({
                'url': proxy_url,
                'port': proxy_port,
                'location': proxy_location
            })
