import requests
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render

# Create your views here.
import re

from django.views.generic import TemplateView
from lxml import html
from rest_framework.decorators import api_view
from rest_framework.response import Response
from selenium import webdriver
from selenium.webdriver import Proxy, FirefoxProfile
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys

from main.models import Proxy, SearchResult


class IndexView(LoginRequiredMixin, TemplateView):
    login_url = '/admin/login/'
    redirect_field_name = 'next'
    template_name = 'home_view.html'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)

        return context


@api_view(['GET'])
def coletar_proxies(request, *args, **kwargs):
    res = requests.get('https://www.proxynova.com/proxy-server-list/country-br/')
    data = res.text
    tree = html.fromstring(data)
    trs = tree.xpath("//tr[@data-proxy-id]")

    proxies = []

    for tr in trs:
        proxy_url = proxy_port = proxy_location = None
        try:
            proxy_url = tr[0][0].get('title')
            proxy_port = re.search('Port (\d+) proxies', tr[1][0].get('title')).group(1)
            proxy_location = tr[5][1][0].text.strip(' - ')
        except:
            pass

        if proxy_location:
            proxies.append({
                'url': proxy_url,
                'port': proxy_port,
                'location': proxy_location
            })


@api_view(['GET'])
def run_test(request, term='bolsonaro', states=['CE', 'DF', 'ES'], **kwargs):
    for uf in states:
        run_search('joao amoedo', p)

    return Response('OK', status=200)


def run_search(term, proxy):
    profile = FirefoxProfile()

    profile.set_preference('network.proxy.type', 1)
    profile.set_preference('network.proxy.http', proxy.url)
    profile.set_preference('network.proxy.http_port', int(proxy.port))
    profile.update_preferences()

    # proxy = Proxy({'proxyType': ProxyType.MANUAL, 'httpProxy': proxies_with_city[0].get('url') + ':' + proxies_with_city[0].get('port')})
    driver = webdriver.Firefox(firefox_profile=profile)
    try:
        driver.set_page_load_timeout(10)
        driver.get("http://www.google.com.br")
        elem = driver.find_element_by_class_name("gsfi")
        elem.send_keys(term)
        elem.send_keys(Keys.RETURN)
        WebDriverWait(driver, 10).until(EC.title_contains(term))

        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "resultStats")))

        SearchResult.objects.create(search_term=term, search_result=driver.page_source, proxy=proxy,
                                    other_data=driver.get_screenshot_as_base64())

    except Exception as err:
        print(err)
        pass
    driver.close()
