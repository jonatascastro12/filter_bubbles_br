from django.db import models
from django.db.models import SET_NULL


class Proxy(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    url = models.CharField(max_length=255)
    port = models.CharField(max_length=255)
    name = models.CharField(max_length=255, null=True, blank=True)

    location = models.CharField(max_length=255, null=True, blank=True)
    uf = models.CharField(max_length=2, null=True, blank=True)
    lat = models.FloatField(null=True, blank=True)
    lng = models.FloatField(null=True, blank=True)

    uptime = models.DecimalField(blank=True, null=True, max_digits=4, decimal_places=2)

    other_data = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.url + ':' + self.port + (('(' + self.location + ')') if self.location else '') + (
            ('(' + self.uf + ')') if self.uf else '')


class SearchResult(models.Model):
    search_iteration = models.ForeignKey('main.SearchIteration', null=True, blank=True, on_delete=models.SET_NULL,
                                         related_name='searches')

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    search_term = models.CharField(max_length=255)
    search_result = models.TextField(null=True, blank=True)
    url = models.CharField(max_length=255, null=True, blank=True)

    proxy = models.ForeignKey(Proxy, on_delete=SET_NULL, null=True, blank=True)

    other_data = models.TextField(null=True, blank=True)

    def image64(self):
        if self.other_data:
            return 'data:image/png;base64,{}'.format(self.other_data.replace("\n", ""))


class SearchIteration(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    search_term = models.CharField(max_length=255)

