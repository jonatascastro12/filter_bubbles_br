from collections import Counter

from celery import shared_task
import json
import requests
import re

from brqq.models import City, GloboVideo, Keyword, CityKeywordCount
from brqq.utilities.cleaners import clean_subtitle


@shared_task(bind=True)
def run_crawler(self):
    response = requests.get('https://especiais.g1.globo.com/o-brasil-que-eu-quero/2018/videos/js/data.json')

    data = response.json()

    i = 0
    municipios = data.get('municipios')
    for mun in municipios:
        i = i + 1

        self.update_state(state='PROGRESS',
                          meta={'current': i, 'total': len(municipios)})

        city = City.objects.get_or_create(name=mun.get('municipio'), uf=mun.get('estado'))[0]

        if (mun.get('video')):
            try:
                if not city.video:

                    video = GloboVideo.objects.filter(globo_id=mun.get('video')).first()

                    if video:
                        city.video = video
                        city.save()
                        continue

                    response2 = requests.get(
                        'https://api.globovideos.com/videos/%s/playlist/callback/wmPlayerPlaylistLoaded%s' % (
                            mun.get('video'), mun.get('video')))
                    videoInfo = response2.text

                    result = re.search('wmPlayerPlaylistLoaded[0-9]+\((.+)\)\;', videoInfo)
                    videoInfo = result.group(1)
                    videoInfo = json.loads(videoInfo).get('videos')[0]

                    video = GloboVideo.objects.create(title=videoInfo.get('title'),
                                                      globo_id=videoInfo.get('id'),
                                                      video_info=json.dumps(videoInfo),
                                                      description=videoInfo.get('description'),
                                                      exhibited_at=videoInfo.get('exhibited_at'),
                                                      )

                    city.video = video
                    city.save()

                    subtitles = [r for r in videoInfo.get('resources') if r.get('type') == 'subtitle']

                    if len(subtitles) > 0 and subtitles[0].get('url'):
                        sub_url = subtitles[0].get('url')
                        video.subtitle_url = sub_url
                        response3 = requests.get(sub_url)

                        video.subtitle_content = response3.content.decode('utf8')

                        video.save()
            except Exception:
                print('error', mun.get('video'))


@shared_task(bind=True)
def run_cleaner(self):
    videos = GloboVideo.objects.filter(subtitle_content__isnull=False).all()

    j = 0
    for video in videos:
        j = j + 1
        video.error_on_clean = False
        video.save()
        # self.update_state(state='PROGRESS',
        #                   meta={'title': 'Cleaner', 'current': j, 'total': len(videos)})

        try:
            cleaned = clean_subtitle(video)
        except Exception as e:

            video.error_on_clean = True
            video.save()


@shared_task(bind=True)
def run_google_nlp(bind=True):
    cities = City.objects.filter(treated_subtitle_part__isnull=False).all()

    for city in cities:
        treated = city.treated_subtitle_part.replace(r'\s+', '')

        words = re.findall(r'(\w+)[\s\,\.]+?', treated)

        wordcount = Counter(words)

        for word in wordcount.items():
            keyword = Keyword.objects.get_or_create(keyword=word[0])[0]

            city_keyword_count = CityKeywordCount.objects.create(keyword=keyword, city=city, count=word[1])
