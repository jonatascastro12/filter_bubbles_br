from django.contrib import admin

# Register your models here.
from django.utils.html import format_html
from django.utils.safestring import mark_safe

from main.models import Proxy, SearchResult


class ProxyAdmin(admin.ModelAdmin):
    model = Proxy
    list_display = [
        'url', 'port', 'location', 'uf'
    ]


class SearchResultsAdmin(admin.ModelAdmin):
    model = SearchResult
    list_display = [
        'created_at', 'search_term', 'proxy', 'thumb',
    ]

    def thumb(self, obj):
        return format_html("<img src='{}' height='300' />", obj.image64())

    thumb.allow_tags = True
    thumb.__name__ = 'Thumb'


admin.site.register(Proxy, ProxyAdmin)
admin.site.register(SearchResult, SearchResultsAdmin)
